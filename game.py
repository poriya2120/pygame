import pygame
pygame.init()
windows=pygame.display.set_mode((600,600))
pygame.display.set_caption("GAME")
x=50
y=40
width=60
height=40
vel=9
isjump=False
jumpCount=10
run=True

while run:
    pygame.time.delay(300)
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            run=False
    
    keys=pygame.key.get_pressed()

    if keys[pygame.K_LEFT] and x>vel:
        x-=vel
    
    if keys[pygame.K_RIGHT] and x<500-height-vel:
        x+=vel
    if not(isjump):
        if keys[pygame.K_UP] and y>vel:
            y-=vel
        if keys[pygame.K_DOWN] and y<500-height-vel:
            y+=vel
        if keys[pygame.K_SPACE]:
            isjump=True
    else:
        if isjump>=-10:
            neg=1
            if jumpCount<0:
                neg=-1
            y-=(jumpCount**2)*0.5*neg
            jumpCount-=1
        else:
            isjump=False
            jumpCount=10
    # windows.fill((255,0,0))
    pygame.draw.rect(windows,(255,0,255),(x,y,width,height))
    pygame.display.update()

pygame.quit()
